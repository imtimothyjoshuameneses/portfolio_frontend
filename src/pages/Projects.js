import React, { useEffect, useState} from 'react';
import Project from '../components/project/Project';

export default function Projects() {

    const[projects, setProjects] = useState([]);

    const fetchProjects = () => {
        fetch(`${process.env.REACT_APP_API_URL}/api/projects/all`)
        .then(response => response.json())
        .then(result => {
            console.log('Fetched projects:', result); // Add this line for debugging
            setProjects(result);
        })
        .catch(error => {
            console.error('Error fetching projects:', error);
        });
    };
    
    useEffect(() => {
        fetchProjects();
    }, []);

    return (
        projects.length > 0 ? <Project projectsData={projects} /> : <div>Loading...</div>
    )
}