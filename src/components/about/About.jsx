import React from 'react'
import "./about.css"
import ME from '../../assets/me1-about.jpg'
import {FaAward} from 'react-icons/fa'
import {FiUsers} from 'react-icons/fi'
import {VscFolderLibrary} from 'react-icons/vsc'

const About = () => {
  return (
    <section id="about">
        <h5 data-aos="fade-down" data-aos-delay="100" data-aos-duration="1000"> Get To Know</h5>
        <h2 data-aos="fade-down" data-aos-delay="100" data-aos-duration="1000"> About Me </h2>

      {/* ABOUT ME  */}
      <div className='container about_container'>

        <div className='about_me' data-aos="fade-right" data-aos-delay="100" data-aos-duration="1000">
          <div className="about_me-image">
            <img src={ME} alt='About Image' />
          </div>
        </div>

        <div className='about_content'>

          <div className='about_cards'>

            <article className='about_card' data-aos="flip-right" data-aos-delay="100" data-aos-duration="1000">
                <FaAward className="about_icon" />
                <h5> Experience </h5>
                <small> 0 years Experience </small>
            </article>
          

            {/* <article className='about_card'>
                <FiUsers className="about_icon" />
                <h5> Clients </h5>
                <small> 200+ clients </small>
            </article> */}

            <a href="#project">
              <article className='about_card' data-aos="flip-left" data-aos-delay="100" data-aos-duration="1000">
                  <VscFolderLibrary className="about_icon" />
                  <h5> Projects </h5>
                  <small> 3+ Completed Projects </small>
              </article>
            </a>

          </div>

          <p data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
          I'm Timothy Joshua Meneses, a recent graduate in BSIT and a former bootcamper at Zuitt. My journey in the world of technology began years ago, and I've always been fascinated by the power of web development. With my newfound skills and knowledge, I'm on a mission to pursue a career as a Full Stack Web Developer.
          <div className='about-breakline'> 
          </div>
          Join me on this exciting journey as I strive to make a positive impact in the world of web development. Together, we can build a future that's both aesthetically pleasing and highly functional, one line of code at a time!
          </p>

          <a href='#contact' className="btn btn-primary"> Let's Talks </a>
        </div>
     </div>

    </section>
  )
}

export default About 