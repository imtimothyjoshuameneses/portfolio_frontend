import React from 'react'
import './testimonials.css'
// import Swiper core and required modules
import {Pagination } from 'swiper/modules';

import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/scrollbar';


const Testimonials = () => {
  return (
    <section id="testimonials">
      <h5> Review from clients </h5>
      <h2> Testimonials </h2>
      
      <Swiper className="container testimonials_container" 
              //Install Swiper modules
              modules={[Pagination]}
              spaceBetween={40}
              slidesPerView={1}
              pagination={{ clickable: true }}
      >

        <SwiperSlide className="testimonial">
          <div className="client_avatar">
            <img src="https://i.pinimg.com/564x/30/6f/d0/306fd0fb64f67ff40f81d8e37f8bf674.jpg" alt="Avatar One" />
          </div>

          <h5 className='client_name'> Ernest Archiever </h5>
          <small className="client_review"> Lorem ipsum dolor sit amet consectetur, adipisicing elit. Reprehenderit voluptates unde id tempora doloribus veritatis aspernatur eum laudantium, cupiditate culpa obcaecati fuga aperiam beatae natus quisquam ullam earum veniam consequuntur.</small>

        </SwiperSlide>

        <SwiperSlide className="testimonial">
          <div className="client_avatar">
            <img src="https://i.pinimg.com/564x/a9/75/93/a975934bb378afc4ca8c133df451f56e.jpg" alt="Avatar One" />
          </div>

          <h5 className='client_name'> Ernest Archiever </h5>
          <small className="client_review"> Lorem ipsum dolor sit amet consectetur, adipisicing elit. Reprehenderit voluptates unde id tempora doloribus veritatis aspernatur eum laudantium, cupiditate culpa obcaecati fuga aperiam beatae natus quisquam ullam earum veniam consequuntur.</small>

        </SwiperSlide>

        <SwiperSlide className="testimonial">
          <div className="client_avatar">
            <img src="https://i.pinimg.com/564x/a9/c7/cc/a9c7cc023b446f3e14d084c9ae4def35.jpg" alt="Avatar One" />
          </div>

          <h5 className='client_name'> Ernest Archiever </h5>
          <small className="client_review"> Lorem ipsum dolor sit amet consectetur, adipisicing elit. Reprehenderit voluptates unde id tempora doloribus veritatis aspernatur eum laudantium, cupiditate culpa obcaecati fuga aperiam beatae natus quisquam ullam earum veniam consequuntur.</small>

        </SwiperSlide>
        
      </Swiper>
    </section>
  )
}

export default Testimonials