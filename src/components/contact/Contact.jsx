import React from 'react'
import './contact.css'
import {MdOutlineEmail} from 'react-icons/md'
import {SiViber} from 'react-icons/si'
import {LiaFacebookMessenger} from 'react-icons/lia'
import { useRef } from 'react';
import emailjs from 'emailjs-com'

const Contact = () => {
  const form = useRef();

  const sendEmail = (e) => {
    e.preventDefault();

    emailjs.sendForm('service_vuiq45p', 'template_1qmz558', form.current, 'f3xMTaHr75Gi-H6sb')
      .then((result) => {
          console.log(result.text);
      }, (error) => {
          console.log(error.text);
      });

      e.target.reset();
  };

  return (
    <section id="contact">
        <h5 data-aos="fade-down" data-aos-delay="100" data-aos-duration="1000"> Get in touch </h5>
        <h2 data-aos="fade-down" data-aos-delay="100" data-aos-duration="1000"> Contact Me </h2>

        <div className="container contact_container">
          <div className="contact_options">

            <article className="contact_option" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
              <MdOutlineEmail className='contact_option-icon ' />
              <h4> Email </h4>
              <h5> imtimothyjoshuameneses@gmail.com </h5>
              <a href="mailto:imtimothyjoshuameneses@gmail.com" target='_blank'> Send a message </a>
            </article>

            <article className="contact_option" data-aos="fade-up" data-aos-delay="250" data-aos-duration="1000">
              <LiaFacebookMessenger className='contact_option-icon '/>
              <h4> Messenger </h4>
              <h5> Timothy Joshua Meneses </h5>
              <a href="https://m.me/timothyjohsua.meneses/" target='_blank'> Send a message </a>
            </article>

            <article className="contact_option" data-aos="fade-up" data-aos-delay="400" data-aos-duration="1000">
              <SiViber className='contact_option-icon '/>
              <h4> Viber </h4>
              <a href="viber://chat?number=+639055928348" target='_blank'> Send a message </a>
            </article>

          </div>
          {/* END OF CONTACT OPTIONS */}
          <form ref={form} onSubmit={sendEmail}>
              <input type="text" name="name" placeholder='Your Full Name' data-aos="fade-left" data-aos-delay="100" data-aos-duration="1000" required />
              <input type="email" name='email' placeholder='Your Email' data-aos="fade-left" data-aos-delay="250" data-aos-duration="1000" required />
              <textarea name="message" rows='7' placeholder='Your Message' data-aos="fade-left" data-aos-delay="400" data-aos-duration="1000" required></textarea>
              <button type='submit' className='btn btn-primary' data-aos="fade-up-left" data-aos-delay="100" data-aos-duration="1000"> Send Message </button>
          </form>
        </div>
    </section>
  )
}

export default Contact