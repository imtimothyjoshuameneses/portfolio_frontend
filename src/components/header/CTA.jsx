import React from 'react'
import Resume from '../../assets/Resume.pdf'

const CTA = () => {
  return (
    <div className='cta'>

        <a href={Resume} download className='btn' data-aos="fade-right" data-aos-delay="100" data-aos-duration="1000"> Download Resume </a>
        <a href='#contact' className='btn btn-primary' data-aos="fade-left" data-aos-delay="100" data-aos-duration="1000"> Let's Talk </a>
    </div>
  )
}

export default CTA   