import React from 'react'
import {BsLinkedin} from 'react-icons/bs'
import {BsGithub} from 'react-icons/bs'
import {FaSquareGitlab} from 'react-icons/fa6'

const HeaderSocials = () => {
  return (
    <div className='header_socials' data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
        <a href="https://www.linkedin.com/in/timothy-joshua-meneses-5b3869231/" target="_blank"> <BsLinkedin/> </a>
        <a href="https://github.com/Tmthymnss" target="_blank"> <BsGithub /> </a>
        <a href="https://gitlab.com/imtimothyjoshuameneses" target="_blank"> <FaSquareGitlab /> </a>
    </div>
  )
}
   
export default HeaderSocials