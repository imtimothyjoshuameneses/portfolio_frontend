import React from 'react'
import './header.css'
import CTA from './CTA'
import ME from '../../assets/image-1b.png'
import HeaderSocials from './HeaderSocials'

const Header = () => {
  return (
    <header>
      <div className='container header_container'>  
        <h5 data-aos="fade-down" data-aos-delay="100" data-aos-duration="1000"> Hey There! I'm </h5>
        <h1 data-aos="fade-down" data-aos-delay="100" data-aos-duration="1000"> Timothy Joshua Meneses </h1> 
        <h5 className='text-light' data-aos="fade-down" data-aos-delay="100" data-aos-duration="1000"> 
        Fullstack Developer 
        </h5> 
        <CTA />

        <HeaderSocials />

        <div className='me' data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
          <img src={ME} alt="me" />
        </div>

    
          <a href="#contact" className='scroll_down'> 
            Scroll Down 
          </a>
       
      </div>
    </header>
  )
}

export default Header