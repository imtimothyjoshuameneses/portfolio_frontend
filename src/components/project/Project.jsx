import React, { useEffect, useState }from 'react'
import './project.css'
import PropTypes from 'prop-types';

export default function Project({projectsData}){
  
  return (
    <section id="project">
      <h5 data-aos="fade-down" data-aos-delay="100" data-aos-duration="1000"> My Recent Works </h5>
      <h2 data-aos="fade-down" data-aos-delay="100" data-aos-duration="1000"> Projects </h2>

      <div className='container project_container' data-aos="zoom-out-up" data-aos-delay="250" data-aos-duration="1000">
        {projectsData?.map(project => (
          <article className='project_items' key={project._id}>
            <div className='project_item-image'>
              <img src={project.image} alt={`Project ${project._id}`} />
            </div>

            <h3>{project.name}</h3>

            <p>{project.description}</p>

            <div className='project_items-cta'>
              <a href={project.gitlabLink} target='_blank' className='btn'>
                Gitlab
              </a>
              <a href={project.deployLink} target='_blank' className='btn btn-primary'>
                Live Demo
              </a>
            </div>
          </article>
        ))}
        
      </div>
    </section>
  )
}

Project.propTypes = {
  projecstData: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
      gitlabLink: PropTypes.string.isRequired,
      deployLink: PropTypes.string.isRequired,
      image: PropTypes.string.isRequired
    })
  )
};
