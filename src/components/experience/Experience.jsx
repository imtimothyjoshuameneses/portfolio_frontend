import React from 'react'
import './experience.css'
import {BsPatchCheckFill} from 'react-icons/bs'

const Experience = () => {
  return (
    <section id="experience">
      <h5 data-aos="fade-down" data-aos-delay="100" data-aos-duration="1000"> What Skills I Have</h5>
      <h2 data-aos="fade-down" data-aos-delay="100" data-aos-duration="1000"> My Experience </h2>
      
      <div className="container experience_container">

        <div className="experience_frontend" data-aos="fade-right" data-aos-delay="100" data-aos-duration="1000">

            <h3> Frontend Development </h3>
            <div className="experience_content">
                <article className="experience_details">
                  <BsPatchCheckFill className='experience_details-icon'/> 
                  <div>
                    <h4> HTML </h4>
                    <small className="text-light"> Hands-On </small>
                  </div>
                </article>

                <article className="experience_details">
                  <BsPatchCheckFill className='experience_details-icon'/> 
                  <div>
                    <h4> CSS </h4>
                    <small className="text-light"> Hands-On </small>
                  </div>
                </article>

                <article className="experience_details">
                  <BsPatchCheckFill className='experience_details-icon'/> 
                  <div>
                    <h4> Bootstrap </h4>
                    <small className="text-light"> Hands-On </small>
                  </div>
                </article>

                <article className="experience_details">
                  <BsPatchCheckFill className='experience_details-icon'/> 
                  <div>
                    <h4> Wireframes </h4>
                    <small className="text-light"> Basic </small>
                  </div>
                </article>

                <article className="experience_details">
                  <BsPatchCheckFill className='experience_details-icon'/> 
                  <div>
                    <h4> ES6 </h4>
                    <small className="text-light"> Hands-On </small>
                  </div>
                </article>

                <article className="experience_details">
                  <BsPatchCheckFill className='experience_details-icon'/> 
                  <div>
                    <h4> Git </h4>
                    <small className="text-light"> Hands-On </small>
                  </div>
                </article>

                <article className="experience_details">
                  <BsPatchCheckFill className='experience_details-icon'/> 
                  <div>
                    <h4> Vercel </h4>
                    <small className="text-light"> Hands-On </small>
                  </div>
                </article>
  
            </div>

        </div>
        
        <div className="experience_backend" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">

            <h3> Backend Development </h3>
            <div className="experience_content">
                <article className="experience_details">
                  <BsPatchCheckFill className='experience_details-icon'/> 
                  <div>
                    <h4> JavaScript </h4>
                    <small className="text-light"> Hands-On </small>
                  </div>
                </article>

                <article className="experience_details">
                  <BsPatchCheckFill className='experience_details-icon'/> 
                  <div>
                    <h4> Node JS </h4>
                    <small className="text-light"> Hands-On </small>
                  </div>
                </article>

                <article className="experience_details">
                  <BsPatchCheckFill className='experience_details-icon'/> 
                  <div>
                    <h4> Express JS </h4>
                    <small className="text-light"> Hands-On </small>
                  </div>
                </article>

                <article className="experience_details">
                  <BsPatchCheckFill className='experience_details-icon'/> 
                  <div>
                    <h4> MongoDB </h4>
                    <small className="text-light"> Hands-On </small>
                  </div>
                </article>

                <article className="experience_details">
                  <BsPatchCheckFill className='experience_details-icon'/> 
                  <div>
                    <h4> REST API </h4>
                    <small className="text-light"> Hands-On </small>
                  </div>
                </article>

                <article className="experience_details">
                  <BsPatchCheckFill className='experience_details-icon'/> 
                  <div>
                    <h4> Postman </h4>
                    <small className="text-light"> Hands-On </small>
                  </div>
                </article>

                <article className="experience_details">
                  <BsPatchCheckFill className='experience_details-icon'/> 
                  <div>
                    <h4> MySQL </h4>
                    <small className="text-light"> Basic </small>
                  </div>
                </article> 
            </div>
        </div>

        <div className="experience_fullstack" data-aos="fade-left" data-aos-delay="100" data-aos-duration="1000">

            <h3> Fullstack Development </h3>
            <div className="experience_content">
                <article className="experience_details">
                  <BsPatchCheckFill className='experience_details-icon'/> 
                  <div>
                    <h4> MongoDB </h4>
                    <small className="text-light"> Hands-On </small>
                  </div>
                </article>

                <article className="experience_details">
                  <BsPatchCheckFill className='experience_details-icon'/> 
                  <div>
                    <h4> Express JS</h4>
                    <small className="text-light"> Hands-On </small>
                  </div>
                </article>

                <article className="experience_details">
                  <BsPatchCheckFill className='experience_details-icon'/> 
                  <div>
                    <h4> React JS </h4>
                    <small className="text-light"> Hands-On </small>
                  </div>
                </article>

                <article className="experience_details">
                  <BsPatchCheckFill className='experience_details-icon'/> 
                  <div>
                    <h4> Node JS </h4>
                    <small className="text-light"> Hands-On </small>
                  </div>
                </article>

                <article className="experience_details">
                  <BsPatchCheckFill className='experience_details-icon'/> 
                  <div>
                    <h4> JavaScript </h4>
                    <small className="text-light"> Hands-On </small>
                  </div>
                </article>

                <article className="experience_details">
                  <BsPatchCheckFill className='experience_details-icon'/> 
                  <div>
                    <h4> DOM Manipulation  </h4>
                    <small className="text-light"> Hands-On </small>
                  </div>
                </article>

                <article className="experience_details">
                  <BsPatchCheckFill className='experience_details-icon'/> 
                  <div>
                    <h4>  API Integration </h4>
                    <small className="text-light"> Basic </small>
                  </div>
                </article> 
            </div>
        </div>

      </div>
    </section>
  )
}

export default Experience