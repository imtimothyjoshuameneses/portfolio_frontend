import React from 'react'
import './footer.css'
import {FaFacebookF} from 'react-icons/fa'
import {BiLogoInstagramAlt} from 'react-icons/bi'
import {FaTwitter} from 'react-icons/fa'

const Footer = () => {
  return (
    <footer>
        <a href="#" className='footer_logo'> TMTHY MNSS</a>

        <ul className="permalinks">
          <li><a href="#"> Home </a></li>
          <li><a href="#about"> About </a></li>
          <li><a href="#experience"> Experience </a></li>
          {/* <li><a href="#services"> Services </a></li> */}
          <li><a href="#portfolio"> Portfolio </a></li>
          {/* <li><a href="#testimonials"> Testimonials </a></li> */}
          <li><a href="#contact"> Contact </a></li>
        </ul>

        <div className="footer_socials">
          <a href="https://www.facebook.com/timothyjohsua.meneses/" target='_blank'> <FaFacebookF/> </a>

          <a href="https://www.instagram.com/tmthymnss/?hl=en" target='_blank'> <BiLogoInstagramAlt/> </a>

          <a href="https://twitter.com/TmthyMnss" target='_blank'> <FaTwitter /> </a>
        </div>

        <div className="footer_copyright">
          <small> &copy; TMTHY MNSS, All rights reserved.  </small>
        </div>
    </footer>
  )
}

export default Footer